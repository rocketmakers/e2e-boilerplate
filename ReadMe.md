# E2E Boilerplate

## Overview

This is an e2e boilerplate using Cypress. 

```bash
make install
make e2e-tests E2E_ACTION=open # opens chrome window
make e2e-tests E2E_ACTION=run # headless
```

We use a .env file for secrets which get injected into cypress envs in the plugin ts.

### Example usage

```typescript
function deleteFutureUpdate(content: string) {
  cy.get(`tr:contains(${content}) button[name="delete-button"]`).click();
  cy.get('input[name="confirmDelete"]').type('delete');
  cy.get('#delete-future-update').click();
}

function createFutureUpdate(content: string, pageId: string) {
  cy.get('#create-future-update').click();
  cy.get('input[name="content"]').type(content);
  cy.get('select[name="pageId"]').select(pageId);
  cy.get('#save-future-update').click();
}

describe('As an admin I need the ability to manage future update information', () => {
  it('Given I log in as an admin, when I create a future update, then I should see the future update', () => {
    cy.login(`${Cypress.env('BASE_URL')}/login`, Cypress.env('USERNAME'), Cypress.env('PASSWORD'));
    cy.get('#future-updates').click();
    const content = `Some incredible feature ${uuid()}`;
    createFutureUpdate(content, 'ageOfProperty');

    cy.get('#future-updates-table').contains(content);
    deleteFutureUpdate(content);
  });
```