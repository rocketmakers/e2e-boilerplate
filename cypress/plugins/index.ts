/* eslint-disable no-param-reassign */
/// <reference types="cypress" />
import { config as dotConfig } from 'dotenv';

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (_: any, config: any) => {
  dotConfig();
  config.env.USERNAME = process.env.USERNAME;
  config.env.PASSWORD = process.env.PASSWORD;
  config.env.BASE_URL = process.env.BASE_URL;
  return config;
};
