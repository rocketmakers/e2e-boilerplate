/// <reference types="cypress" />

import 'cypress-file-upload';
import 'cypress-localstorage-commands';
import { radioInput, textInput } from './helpers';

Cypress.Commands.add('login', (url: string, email: string, password: string) => {
  cy.visit(url);

  cy.wait(1000);

  cy.url().then(currentUrl => {
    // Check if already logged in
    if (!currentUrl.includes('/home')) {
      cy.get('input[name="email"]').type(email);
      cy.get('input[name="password"]').type(password);
      cy.get('.btn')
        .contains('Login')
        .click();
    }
  });

  cy.url().should('include', '/home');
});

Cypress.Commands.add('failedLogin', (url: string, email: string, password: string) => {
  cy.visit(url);

  cy.wait(1000);

  cy.url().then(currentUrl => {
    // Check if already logged in
    if (!currentUrl.includes('/home')) {
      cy.get('input[name="email"]').type(email);
      cy.get('input[name="password"]').type(password);
      cy.get('.btn')
        .contains('Login')
        .click();
    }
  });

  cy.wait(1000);

  cy.url().should('include', '/login');
});

Cypress.Commands.add('logout', () => {
  cy.get('.btn')
    .contains('Log Out')
    .click();
});

Cypress.Commands.add('logoutIfAlreadyLoggedIn', appBaseUrl => {
  cy.visit(`${appBaseUrl}/login`);

  cy.wait(1000);

  cy.url().then(currentUrl => {
    if (!currentUrl.includes('/login')) {
      cy.logout();
    }
  });
});