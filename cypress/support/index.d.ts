/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-cy attribute.
     * @example cy.dataCy('greeting')
     */
    login(url: string, email: string, password: string): Chainable<Element>;
    failedLogin(url: string, email: string, password: string): Chainable<Element>;
    logoutIfAlreadyLoggedIn(appBaseUrl: string): Chainable<Element>;
    logout(): Chainable<Element>;

    /**
     * Helpers
     */
    testTextInput(name: string, value: string | number, validate: boolean): Chainable<Element>;
  }
}
