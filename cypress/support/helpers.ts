/// <reference types="cypress" />

import 'cypress-file-upload';
import 'cypress-localstorage-commands';

//--------------------------------------------------------------------
//
// Helper Functions
//
//--------------------------------------------------------------------

/**
 * Get Text Input, Type value and Validate
 * @param name - name of the input
 * @param value - content of the input
 * @param validate - whether the input gets validated after entry
 */
export function textInput<T>(name: keyof T, value: string, validate: boolean) {
  cy.get(`input[name="${name}"]`).type(value);
  if (validate) {
    cy.get(`input[name="${name}"]`).should('have.value', value);
  }
}

/**
 * Get Text Area Input, Type value and Validate
 * @param name - name of the input
 * @param value - content of the input
 * @param validate - whether the input gets validated after entry
 */
export function textAreaInput<T>(name: keyof T, value: string, validate: boolean) {
  cy.get(`textarea[name="${name}"]`).type(value);
  if (validate) {
    cy.get(`textarea[name="${name}"]`).should('have.value', value);
  }
}

/**
 * Get Select Input, select the value and Validate
 * @param name - name of the input
 * @param value - content of the input
 * @param validate - whether the input gets validated after entry
 */
export function selectInput<T>(name: keyof T, value: string, validate: boolean) {
  cy.get(`select[name="${name}"]`).select(value);
  if (validate) {
    cy.get(`select[name="${name}"]`).should('have.value', value);
  }
}

/**
 * Get Radio Input, select the value and Validate
 * @param name - name of the input
 * @param value - content of the input
 * @param validate - whether the input gets validated after entry
 */
export function radioInput<T>(name: keyof T, value: string) {
  cy.get(`input[name="${name}"]`)
    .siblings('label.radio-label')
    .contains(value)
    .click();
}

/**
 * Get Checkbox Input, select the value and Validate
 * @param name - name of the input
 * @param value - content of the input
 * @param validate - whether the input gets validated after entry
 */
export function checkboxInput<T>(name: keyof T, value: string, validate: boolean) {
  cy.get(`input[name="${name}"]`).click();
  if (validate) {
    cy.get(`input[name="${name}"]`).should('have.value', 'on');
  }
}
