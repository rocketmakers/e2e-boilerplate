/// <reference types="Cypress" />
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../support/index.d.ts" />

describe('Login', () => {
  it('As an user, with correct credentials I should be able to login to the app', () => {
    cy.login(`${Cypress.env('BASE_URL')}/login`, Cypress.env('USERNAME'), Cypress.env('PASSWORD'));
    cy.logout();
  });
});
