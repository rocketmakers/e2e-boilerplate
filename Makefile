#############################
# VARIABLES
#############################
START 										:= $(shell printf "\033[34;1m")
END 											:= $(shell printf "\033[0m")

PROJECT_NAME=yours
NODE_MODULES=./node_modules
CYPRESS=./node_modules/cypress/bin/cypress
E2E_ACTION ?= open

#############################
# CUSTOM FUNCTIONS
#############################
define header
  $(info $(START)▶▶▶ $(1)$(END))
endef

#############################
# BASE COMMANDS
#############################
install:
	$(call header,Installing...)
	npm install

e2e-tests:
	$(call header,E2E ...)
	 $(CYPRESS) $(E2E_ACTION)